package com.sopra.config.desarrollo.config;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.test.util.ReflectionTestUtils;

import com.sopra.config.desarrollo.kafka.Receiver;

@RunWith(MockitoJUnitRunner.class)
public class ReceiverConfigTest {
	
	@InjectMocks
	ReceiverConfig receiverConfig;
	
	private String bootstrapServers = "bootstrapServers";
	private String groupId = "groupId";
	private String autoOffSetReset = "autoOffSetReset";
	
	@Before
	public void init() {
	    MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(receiverConfig,"bootstrapServers", bootstrapServers);
		ReflectionTestUtils.setField(receiverConfig,"groupId", groupId);
		ReflectionTestUtils.setField(receiverConfig,"autoOffSetReset", autoOffSetReset);
	}
	
	@Test
	public void consumerConfigs() {
		Map<String, Object> props = receiverConfig.consumerConfigs();
		
		assertNotNull(props);
		
		validateConfig(props);
	}

	private void validateConfig(Map<String, Object> props) {
		assertTrue(props.get(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG).toString().equals(bootstrapServers));
		assertTrue(props.get(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG).equals(StringDeserializer.class));
		assertTrue(props.get(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG).equals(StringDeserializer.class));
		assertTrue(props.get(ConsumerConfig.GROUP_ID_CONFIG).toString().equals(groupId));
		assertTrue(props.get(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG).toString().equals(autoOffSetReset));
	}
	
	@Test
	public void consumerFactory() {
		ConsumerFactory<String, String> factory = receiverConfig.consumerFactory();
		
		assertNotNull(factory);
		
		validateConfig(factory.getConfigurationProperties());
	}
	
	@Test
	public void kafkaListenerContainerFactory() {
		KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, String>> container = receiverConfig.kafkaListenerContainerFactory();
		
		assertNotNull(container);
	}
	
	@Test
	public void receiver() {
		Receiver receiver = receiverConfig.receiver();
		
		assertNotNull(receiver);
		
		assertTrue((""+receiver.getLatch().getCount()).equals("1"));
	}
}
