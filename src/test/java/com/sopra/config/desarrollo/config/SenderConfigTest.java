package com.sopra.config.desarrollo.config;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.test.util.ReflectionTestUtils;

import com.sopra.config.desarrollo.kafka.Sender;

@RunWith(MockitoJUnitRunner.class)
public class SenderConfigTest {
	
	@InjectMocks
	SenderConfig senderConfig;
	
	private String bootstrapServers = "bootstrapServers";
	
	@Before
	public void init() {
	    MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(senderConfig,"bootstrapServers", bootstrapServers);
	}
	
	@Test
	public void consumerConfigs() {
		KafkaTemplate<String, String> kafkaTemplate = senderConfig.kafkaTemplate();
		
		assertNotNull(kafkaTemplate);
	}

	@Test
	public void producerFactory() {
		ProducerFactory<String, String> produces = senderConfig.producerFactory();
		
		assertNotNull(produces);
	}
	
	@Test
	public void producerConfigs() {
		Map<String, Object> props = senderConfig.producerConfigs();
		
		assertNotNull(props);
		
		assertTrue(props.get(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG).toString().equals(bootstrapServers));
		assertTrue(props.get(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG).equals(StringSerializer.class));
		assertTrue(props.get(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG).equals(StringSerializer.class));
	}
	
	@Test
	public void sender() {
		Sender sender = senderConfig.sender();
		
		assertNotNull(sender);
	}
}
