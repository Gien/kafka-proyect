package com.sopra.config.desarrollo.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doThrow;

import java.util.concurrent.CountDownLatch;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.sopra.config.desarrollo.kafka.Receiver;
import com.sopra.config.desarrollo.kafka.Sender;

@RunWith(MockitoJUnitRunner.class)
public class SendMsgControllerTest {
	
	@InjectMocks
	SendMsgController sendMsgController;
	
	InterruptedException exception = new InterruptedException("Exception");
	
	@Mock Sender sender;
	
	@Mock Receiver receiver;
	
	private CountDownLatch latch = new CountDownLatch(1);

	private static final String RESULT = "OK";
	
	@Test
	public void get() {
		sendMsgController.get("probando");
		assertEquals("OK", RESULT);
	}
	
	@Test
	public void getMessage() {
		when(receiver.getLatch()).thenReturn(latch);
		
		sendMsgController.getMessage();
		assertEquals("OK", RESULT);
	}
	
	@Test
	public void getMessage_throw() {
		sendMsgController.getMessage();
		assertEquals("OK", RESULT);
	}
	
}
