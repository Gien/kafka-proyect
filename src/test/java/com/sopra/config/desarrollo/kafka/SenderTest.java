package com.sopra.config.desarrollo.kafka;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.kafka.core.KafkaTemplate;

@RunWith(MockitoJUnitRunner.class)
public class SenderTest {
	
	@InjectMocks
	Sender sender;
	
	@Mock
	private KafkaTemplate<String, String> kafkaTemplate;
	
	private static final String RESULT = "OK";
	private String topic = "topic";
	private String payload = "payload";

	@Test
	public void getLatch() {
		sender.send(topic, payload);
	}

}
