package com.sopra.config.desarrollo.kafka;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ReceiverTest {
	
	@InjectMocks
	Receiver receiver;
	
	private static final String RESULT = "OK";
	private String message = "message";

	@Test
	public void getLatch() {
		assertEquals(receiver.getLatch().getCount(), 1);
	}

	@Test
	public void receive() {
		receiver.receive(message);
		assertTrue(receiver.getMessage().equals(message));
		assertEquals(receiver.getLatch().getCount(), 0);
	}
}
