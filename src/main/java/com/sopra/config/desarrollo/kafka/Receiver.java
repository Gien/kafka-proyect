package com.sopra.config.desarrollo.kafka;

import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;

public class Receiver {
	private static final Logger LOGGER = LoggerFactory.getLogger(Receiver.class);
	
	private String message = "";
	private CountDownLatch latch = new CountDownLatch(1);
	
	public CountDownLatch getLatch() {
		return latch;
	}

	@KafkaListener(topics = "${kafka.topic.kafkatest}")
	public void receive(String payload) {
	    LOGGER.info("received payload='{}'", payload);
	    message = payload;
	    latch.countDown();
	}
	
	public String getMessage() {
		return message;
	}
}
