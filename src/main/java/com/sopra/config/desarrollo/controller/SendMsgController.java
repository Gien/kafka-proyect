package com.sopra.config.desarrollo.controller;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sopra.config.desarrollo.kafka.Receiver;
import com.sopra.config.desarrollo.kafka.Sender;

@RestController
public class SendMsgController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SendMsgController.class);
	
	@Autowired private Sender sender;

	@Autowired private Receiver receiver;
	
	@Value("${kafka.topic.kafkatest}")
	private String topic;
	
	@RequestMapping(path="/sendmsg/{message}",
			consumes={MediaType.APPLICATION_JSON_VALUE},
			produces={MediaType.APPLICATION_JSON_VALUE},
			method=RequestMethod.GET)
	public void get(@PathVariable("message") String message) {
		LOGGER.info("Enviando mensaje='{}'", message);
		sender.send(topic, message);
	}
	
	@RequestMapping(path="/getmsg/",
			consumes={MediaType.APPLICATION_JSON_VALUE},
			produces={MediaType.APPLICATION_JSON_VALUE},
			method=RequestMethod.GET)
	public void getMessage() {
	    try {
			receiver.getLatch().await(10000, TimeUnit.MILLISECONDS);
			LOGGER.info("Mensaje recibido: {}", receiver.getMessage());
		} catch (Exception e) {
			LOGGER.error("Error recibiendo message: {}", e.getMessage());
		}
	}
}
