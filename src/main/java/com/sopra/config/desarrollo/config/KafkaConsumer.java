package com.sopra.config.desarrollo.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {
	private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConsumer.class);

	private int count = 1;

	@KafkaListener(topics = "${kafka.topic.kafkatest}")
	public void receiveMessage(String payload) {
		LOGGER.info("<{}> Received <{}>", count, payload);
		count++;
	  }

}
