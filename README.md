# kafka-proyect

Envio y consumo de mensajes en kafka.

Para realizar este modulo hemos utilizado la guía de instalación de kafka: [DZone](https://dzone.com/articles/running-apache-kafka-on-windows-os)

### Descargando los componentes:

 * [JDK 1.8] - u181
 * [Zookeeper] - 3.4.13
 * [Kafka] - 2.11-2.0.0


Generamos un restController como api expuesta.
 
Realizamos 2 clases que enviaran (Sender) y recibiran(Receiver) los mensajes de la cola kafka.
 
Posteriormente hacemos 2 clases de configuración para invocar al Sender y al Receiver, denomindas SenderConfig y ReceiverConfig, generando los Bean de de acceso a la cola.
 
En el SenderConfig, generamos un Bean Sender, que invocara a nuestra clase Sender, para poder enviar el mensaje de forma directa. Posteriormente bastará con traernos el Sender del contexto de Spring.
 
En el ReceiverConfig, realizamos la misma operación y generamos un Bean Receiver, que invocará a nuestra clase Receiver, bastará con traernos el objeto del contexto de Spring para leer la cola y recogerá los mensajes.
 
2 peculiaridades:
 
   - La clase Sender recibira el topic y el mensaje a enviar el envio se realizará mediante el objeto KafkaTemplate
   - La clase Receiver tendrá un metodo con anotación de kafka para poder leer la cola, al cual debemos indicarle el topic de lectura. (@KafkaListener)
 
 
 
 
 
 
 
 
> En resources se adjunta el postman 
> a ejecutar, para el envio del mensaje 
 
 
 
 [JDK 1.8]: <https://www.oracle.com/technetwork/es/java/javase/downloads/jdk8-downloads-2133151.html>
 [Kafka]: <http://kafka.apache.org/downloads.html>
 [Zookeeper]: <http://zookeeper.apache.org/releases.html>
 
 